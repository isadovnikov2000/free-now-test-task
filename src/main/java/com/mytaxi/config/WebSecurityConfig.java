package com.mytaxi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

/*
 * Configuration for Spring Security
 * all authentication request checks
 */
//@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private DataSource dataSource;

  @Value("${spring.security.select.users.query}")
  private String usersQuery;

  @Value("${spring.security.select.roles.query}")
  private String rolesQuery;

  @Override
  protected void configure(HttpSecurity http) throws Exception{

    http.csrf().disable()
        .authorizeRequests()
        .antMatchers("/**").hasAnyRole("USER")
        .anyRequest().authenticated()
        .and()
        .formLogin()
        .loginPage("/login")
        .permitAll()
        .and()
        .logout()
        .permitAll();
  }


  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

    auth.jdbcAuthentication().dataSource(dataSource)
        .usersByUsernameQuery(usersQuery)
        .authoritiesByUsernameQuery(rolesQuery);


  }

}
