package com.mytaxi.domainobject;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(
    name = "manufacturer",
    uniqueConstraints = @UniqueConstraint(name = "uc_name", columnNames = {"name"})
)
public class ManufacturerDO extends BasicDO
{

    @Column(nullable = false)
    @NotNull(message = "Name plate can not be null!")
    private String name;

    private ManufacturerDO() {
    }

    public ManufacturerDO(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
