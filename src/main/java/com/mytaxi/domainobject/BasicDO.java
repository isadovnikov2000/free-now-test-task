package com.mytaxi.domainobject;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Basic Object for all entities
 */
@MappedSuperclass
abstract class BasicDO {

  @Id
  @GeneratedValue
  protected Long id;

  public Long getId() {
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    BasicDO basicDO = (BasicDO) o;

    return getId() != null ? getId().equals(basicDO.getId()) : basicDO.getId() == null;

  }

  @Override
  public int hashCode() {
    return getId() != null ? getId().hashCode() : 0;
  }
}
