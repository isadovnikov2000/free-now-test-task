package com.mytaxi.domainobject;

import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.GeoCoordinate;
import com.mytaxi.domainvalue.OnlineStatus;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Table(
    name = "car",
    uniqueConstraints = @UniqueConstraint(name = "uc_license_plate", columnNames = {"licensePlate"})
)
public class CarDO extends BasicDO
{

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime dateCreated = ZonedDateTime.now();

    @Column(nullable = false)
    @NotNull(message = "License plate can not be null!")
    private String licensePlate;

    @Column(nullable = false)
    @NotNull(message = "Seat count can not be null!")
    private Integer seatCount;

    @Column(nullable = false)
    private Boolean convertible = false;

    @Column(nullable = false)
    private Boolean isAvailable = false;

    @Column(nullable = false)
    private Double rating;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EngineType engineType;

    @ManyToOne()
    @JoinColumn(name = "manufacturer.id")
    @NotNull(message = "Manufacturer can not be null!")
    private ManufacturerDO manufacturer;


    private CarDO()
    {
    }


    public CarDO(String licensePlate, Integer seatCount, ManufacturerDO manufacturer)
    {
        this.licensePlate = licensePlate;
        this.seatCount = seatCount;
        this.manufacturer = manufacturer;
        this.convertible = false;
        this.rating = 5.0;
        this.engineType = EngineType.GAS;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Integer getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(Integer seatCount) {
        this.seatCount = seatCount;
    }

    public Boolean getConvertible() {
        return convertible;
    }

    public void setConvertible(Boolean convertible) {
        this.convertible = convertible;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public EngineType getEngineType() {
        return engineType;
    }

    public void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    public ManufacturerDO getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(ManufacturerDO manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarDO carDO = (CarDO) o;

        if (getId() != null ? !getId().equals(carDO.getId()) : carDO.getId() != null) return false;
        return getLicensePlate() != null ? getLicensePlate().equals(carDO.getLicensePlate()) : carDO.getLicensePlate() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getLicensePlate() != null ? getLicensePlate().hashCode() : 0);
        return result;
    }
}
