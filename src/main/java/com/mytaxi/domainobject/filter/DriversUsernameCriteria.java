package com.mytaxi.domainobject.filter;

import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainobject.filter.core.Criteria;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class DriversUsernameCriteria implements Criteria<DriverDO> {

  private String name;

  public DriversUsernameCriteria(String name) {
    this.name = name;
  }

  @Override
  public List<DriverDO> filter(Iterable<DriverDO> drivers) {
    Iterator<DriverDO> i = drivers.iterator();
    List<DriverDO> result = new LinkedList<>();

    while (i.hasNext()) {
      DriverDO driver = i.next();
      if(this.name.equalsIgnoreCase(driver.getUsername())) {
        result.add(driver);
      }
    }

    return result;
  }

}
