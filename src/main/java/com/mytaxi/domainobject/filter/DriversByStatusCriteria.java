package com.mytaxi.domainobject.filter;

import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainobject.filter.core.Criteria;
import com.mytaxi.domainvalue.OnlineStatus;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class DriversByStatusCriteria implements Criteria<DriverDO> {

  private OnlineStatus status;

  public DriversByStatusCriteria(OnlineStatus status) {
    this.status = status;
  }

  @Override
  public List<DriverDO> filter(Iterable<DriverDO> drivers) {
    Iterator<DriverDO> i = drivers.iterator();
    List<DriverDO> result = new LinkedList<>();

    while (i.hasNext()) {
      DriverDO driver = i.next();
      if(this.status.equals(driver.getOnlineStatus())) {
        result.add(driver);
      }
    }

    return result;
  }

}
