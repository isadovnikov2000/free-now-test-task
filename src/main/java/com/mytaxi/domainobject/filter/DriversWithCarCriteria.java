package com.mytaxi.domainobject.filter;

import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainobject.filter.core.Criteria;

import java.util.*;

public class DriversWithCarCriteria implements Criteria<DriverDO> {

  @Override
  public List<DriverDO> filter(Iterable<DriverDO> drivers) {
    Iterator<DriverDO> i = drivers.iterator();
    List<DriverDO> result = new LinkedList<>();

    while (i.hasNext()) {
      DriverDO driver = i.next();
      if(driver.getCar() != null) {
        result.add(driver);
      }
    }

    return result;
  }
}
