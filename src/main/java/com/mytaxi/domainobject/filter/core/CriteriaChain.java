package com.mytaxi.domainobject.filter.core;

import javafx.util.Pair;

import java.util.LinkedList;
import java.util.List;


/*
 * Filter all results by all criteria
 */
public class CriteriaChain <T> {

  //LinkedList because i want executing one by one(FIFO)
  private Criteria<T> firstFilter;
  private List<Pair<CriteriaType, Criteria<T>>> filters;

  public CriteriaChain(Criteria<T> criteria) {
    filters = new LinkedList<>();
    firstFilter = criteria;
  }

  public List<T> process(Iterable<T> values) {
    List<T> result = firstFilter.filter(values);

    for(Pair<CriteriaType, Criteria<T>> pair : filters) {
      CriteriaType type = pair.getKey();
      Criteria<T> filter = pair.getValue();

      if(CriteriaType.AND.equals(type)) {
        result = filter.filter(result);
      } else {
        List<T> orValues = filter.filter(values);

        for(T value : orValues) {
          if(!result.contains(value)) {
            result.add(value);
          }
        }
      }
    }

    return result;
  }

  public void adCriteria(CriteriaType type, Criteria<T> criteria) {
    Pair<CriteriaType, Criteria<T>> pair = new Pair<>(type, criteria);
    filters.add(pair);
  }

  public enum CriteriaType {
    AND, OR
  }
}
