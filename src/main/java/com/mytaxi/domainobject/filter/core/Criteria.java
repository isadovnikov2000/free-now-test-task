package com.mytaxi.domainobject.filter.core;

import java.util.List;

public interface Criteria<T> {
  List<T> filter(Iterable<T> objects);
}
