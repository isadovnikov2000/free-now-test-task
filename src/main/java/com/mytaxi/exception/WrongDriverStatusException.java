package com.mytaxi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Driver has wrong status.")
public class WrongDriverStatusException extends BasicException
{
    static final long serialVersionUID = -3387516993334229948L;


    public WrongDriverStatusException(String message)
    {
        super(message);
    }

}
