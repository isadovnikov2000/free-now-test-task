package com.mytaxi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid value of variable.")
public class InvalidValueException extends BasicException
{
    static final long serialVersionUID = -3387516993334229948L;


    public InvalidValueException(String message)
    {
        super(message);
    }

}
