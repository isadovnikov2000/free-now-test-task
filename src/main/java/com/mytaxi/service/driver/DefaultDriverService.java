package com.mytaxi.service.driver;

import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainobject.filter.DriversByStatusCriteria;
import com.mytaxi.domainobject.filter.DriversUsernameCriteria;
import com.mytaxi.domainobject.filter.core.Criteria;
import com.mytaxi.domainobject.filter.DriversWithCarCriteria;
import com.mytaxi.domainobject.filter.core.CriteriaChain;
import com.mytaxi.domainvalue.GeoCoordinate;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import java.util.List;

import com.mytaxi.exception.WrongDriverStatusException;
import com.mytaxi.service.car.CarService;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
public class DefaultDriverService implements DriverService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultDriverService.class);

    private final DriverRepository driverRepository;
    private final CarService carService;


    public DefaultDriverService(final DriverRepository driverRepository,
                                final CarService carService)
    {
        this.driverRepository = driverRepository;
        this.carService = carService;
    }


    /**
     * Selects a driver by id.
     *
     * @param driverId
     * @return found driver
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    public DriverDO find(Long driverId) throws EntityNotFoundException
    {
        return findDriverChecked(driverId);
    }


    /**
     * Creates a new driver.
     *
     * @param driverDO
     * @return
     * @throws ConstraintsViolationException if a driver already exists with the given username, ... .
     */
    @Override
    public DriverDO create(DriverDO driverDO) throws ConstraintsViolationException
    {
        DriverDO driver;
        try
        {
            driver = driverRepository.save(driverDO);
        }
        catch (DataIntegrityViolationException e)
        {
            LOG.warn("Some constraints are thrown due to driver creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return driver;
    }


    /**
     * Deletes an existing driver by id.
     *
     * @param driverId
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    @Transactional
    public void delete(Long driverId) throws EntityNotFoundException
    {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setDeleted(true);
    }


    /**
     * Update the location for a driver.
     *
     * @param driverId
     * @param longitude
     * @param latitude
     * @throws EntityNotFoundException
     */
    @Override
    @Transactional
    public void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException
    {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setCoordinate(new GeoCoordinate(latitude, longitude));
    }

    @Override
    @Transactional
    public void manageCar(long driverId, long carId, boolean isSelect) {
        DriverDO driver = findDriverChecked(driverId);

        if(OnlineStatus.ONLINE.equals(driver.getOnlineStatus())) {
            CarDO car = carService.find(carId);
            CarDO driverCar = driver.getCar();

            boolean isAvailable = car.getAvailable();
            boolean hasCar = driverCar != null;

            if(isSelect) {
                if (isAvailable) {
                    if (hasCar) {
                        //driver with car trying to select car
                        throw new CarAlreadyInUseException("Driver [" + driver.getId() + "] already has car");
                    } else {
                        //everything is okay
                        car.setAvailable(false);
                        driver.setCar(car);
                    }
                } else {
                    if (!(hasCar && driverCar.getId().equals(carId))) {
                        //trying to get access to another driver car
                        throw new CarAlreadyInUseException("Car [" + driver.getId() + "] already in use");
                    }
                }
            } else {
                if (!isAvailable) {
                    if (hasCar && driverCar.getId().equals(carId)) {
                        //everything is okay
                        car.setAvailable(true);
                        driver.setCar(null);
                    } else {
                        //trying to get access to another driver car
                        throw new CarAlreadyInUseException("Car [" + driver.getId() + "] already in use");
                    }
                }
            }
        } else {
            throw new WrongDriverStatusException("Driver [" + driver.getId() + "] offline");
        }
    }


    /**
     * Find all drivers by online state.
     *
     * @param onlineStatus
     */
    @Override
    public List<DriverDO> find(OnlineStatus onlineStatus)
    {
//        Of course better to filter results on DB layer
//        CriteriaChain<DriverDO> chain = new CriteriaChain<>(new DriversByStatusCriteria(onlineStatus));
//        Iterable<DriverDO> driverDOs = driverRepository.findAll();
//        return chain.process(driverDOs);
        return driverRepository.findByOnlineStatus(onlineStatus);
    }


    @Override
    public List<DriverDO> findByNameOrWithCars(String name)
    {
        CriteriaChain<DriverDO> chain = new CriteriaChain<>(new DriversWithCarCriteria());
        chain.adCriteria(CriteriaChain.CriteriaType.OR, new DriversUsernameCriteria(name));

        Iterable<DriverDO> driverDOs = driverRepository.findAll();

        return chain.process(driverDOs);
    }


    private DriverDO findDriverChecked(Long driverId) throws EntityNotFoundException
    {
        DriverDO driverDO = driverRepository.findOne(driverId);
        if (driverDO == null)
        {
            throw new EntityNotFoundException("Could not find entity with id: " + driverId);
        }
        return driverDO;
    }

}
