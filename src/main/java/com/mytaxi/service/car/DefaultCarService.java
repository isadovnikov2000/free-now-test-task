package com.mytaxi.service.car;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.driver.DefaultDriverService;
import com.mytaxi.service.manufacturer.ManufacturerService;
import com.mytaxi.util.Constants;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class DefaultCarService implements CarService {


    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultDriverService.class);

    private final CarRepository carRepository;
    private final ManufacturerService manufacturerService;


    public DefaultCarService(final CarRepository carRepository,
                             final ManufacturerService manufacturerService
    )
    {
        this.manufacturerService = manufacturerService;
        this.carRepository = carRepository;
    }


    @Override
    public CarDO find(Long carId) throws EntityNotFoundException {
        return carRepository.findOne(carId);
    }

    @Override
    public CarDO create(CarDO carDO) throws ConstraintsViolationException {
        CarDO car;
        try
        {
            ManufacturerDO manufacturerDO = manufacturerService.find(carDO.getManufacturer().getId());
            carDO.setManufacturer(manufacturerDO);
            carDO.setRating(Constants.DEFAULT_RATING);

            car = carRepository.save(carDO);
        }
        catch (DataIntegrityViolationException | EntityNotFoundException e)
        {
            LOG.warn("Some constraints are thrown due to car creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        catch (Exception e)
        {
            LOG.error(e.getMessage(), e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return car;
    }

    @Override
    public void delete(Long carId) throws EntityNotFoundException {
        carRepository.delete(carId);
    }
}
