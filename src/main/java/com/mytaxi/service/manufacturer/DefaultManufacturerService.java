package com.mytaxi.service.manufacturer;

import com.mytaxi.dataaccessobject.ManufacturerRepository;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.driver.DefaultDriverService;
import com.mytaxi.service.manufacturer.ManufacturerService;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class DefaultManufacturerService implements ManufacturerService {


    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultDriverService.class);

    private final ManufacturerRepository manufacturerRepository;

    public DefaultManufacturerService(final ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public ManufacturerDO find(Long manufactureId) throws EntityNotFoundException {
        if(manufactureId == null)
            throw new IllegalArgumentException("Manufacture id cant be NULL");

        ManufacturerDO manufacturerDO = manufacturerRepository.findOne(manufactureId);
        if(manufacturerDO != null)
            return manufacturerDO;
        else
            throw new EntityNotFoundException("Manufacture with id [" + manufactureId + "] not exist");
    }

    @Override
    public ManufacturerDO create(ManufacturerDO manufactureDO) throws ConstraintsViolationException {
        ManufacturerDO manufacturer;
        try
        {
            manufacturer = manufacturerRepository.save(manufactureDO);
        }
        catch (DataIntegrityViolationException e)
        {
            LOG.warn("Some constraints are thrown due to manufacturer creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return manufacturer;
    }

    @Override
    public void delete(Long manufactureId) throws EntityNotFoundException {
        manufacturerRepository.delete(manufactureId);
    }
}
