package com.mytaxi.service.manufacturer;

import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;

public interface ManufacturerService {

    ManufacturerDO find(Long manufactureId) throws EntityNotFoundException;

    ManufacturerDO create(ManufacturerDO manufactureDO) throws ConstraintsViolationException;

    void delete(Long manufactureId) throws EntityNotFoundException;

}
