package com.mytaxi.controller;

import com.mytaxi.controller.mapper.ManufacturerMapper;
import com.mytaxi.datatransferobject.ManufacturerDTO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.manufacturer.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * All operations with a manufacturer will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("v1/manufacturer")
public class ManufacturerController
{

    private final ManufacturerService manufacturerService;


    @Autowired
    public ManufacturerController(final ManufacturerService manufacturerService)
    {
        this.manufacturerService = manufacturerService;
    }


    @GetMapping("/{manufacturerId}")
    public ManufacturerDTO getManufacturer(@Valid @PathVariable long manufacturerId) throws EntityNotFoundException
    {
        return ManufacturerMapper.makeManufacturerDTO(manufacturerService.find(manufacturerId));
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ManufacturerDTO createManufacturer(@Valid @RequestBody ManufacturerDTO manufacturerDTO) throws ConstraintsViolationException
    {
        ManufacturerDO manufacturerDO = ManufacturerMapper.makeManufacturerDO(manufacturerDTO);
        return ManufacturerMapper.makeManufacturerDTO(manufacturerService.create(manufacturerDO));
    }


    @DeleteMapping("/{manufacturerId}")
    public void deleteManufacturer(@Valid @PathVariable long manufacturerId) throws EntityNotFoundException
    {
        manufacturerService.delete(manufacturerId);
    }


}
