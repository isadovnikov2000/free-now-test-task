package com.mytaxi.controller.mapper;

import com.mytaxi.datatransferobject.ManufacturerDTO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.domainvalue.GeoCoordinate;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ManufacturerMapper
{
    public static ManufacturerDO makeManufacturerDO(ManufacturerDTO manufacturerDTO)
    {
        return new ManufacturerDO(manufacturerDTO.getName());
    }


    public static ManufacturerDTO makeManufacturerDTO(ManufacturerDO manufacturerDTO)
    {
        ManufacturerDTO.ManufacturerDTOBuilder driverDTOBuilder = ManufacturerDTO.newBuilder()
            .setId(manufacturerDTO.getId())
            .setName(manufacturerDTO.getName());
        return driverDTOBuilder.createManufacturerDTO();
    }


    public static List<ManufacturerDTO> makeManufacturerDTOList(Collection<ManufacturerDO> drivers)
    {
        return drivers.stream()
            .map(ManufacturerMapper::makeManufacturerDTO)
            .collect(Collectors.toList());
    }
}
