package com.mytaxi.controller.mapper;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.datatransferobject.ManufacturerDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.GeoCoordinate;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CarMapper
{
    public static CarDO makeCarDO(CarDTO car)
    {
        ManufacturerDO aDo = new ManufacturerDO("");
        aDo.setId(car.getManufacturerId());

        CarDO carDO = new CarDO(car.getLicensePlate(), car.getSeatCount(), aDo);
        carDO.setConvertible(car.getConvertible());
        carDO.setEngineType(EngineType.valueOf(car.getEngineType()));
        carDO.setRating(car.getRating());

        return carDO;
    }


    public static CarDTO makeCarDTO(CarDO carDO)
    {

        CarDTO.CarDTOBuilder CarDTOBuilder = CarDTO.newBuilder()
            .setId(carDO.getId())
            .setLicensePlate(carDO.getLicensePlate())
            .setConvertible(carDO.getConvertible())
            .setSeatCount(carDO.getSeatCount())
            .setEngineType(carDO.getEngineType().name())
            .setRating(carDO.getRating());

        ManufacturerDO manufacturer = carDO.getManufacturer();
        if (manufacturer != null)
        {
            ManufacturerDTO aDo = ManufacturerMapper.makeManufacturerDTO(manufacturer);
            CarDTOBuilder.setManufacturerId(aDo.getId());
        }

        return CarDTOBuilder.createCarDTO();
    }


    public static List<CarDTO> makeCarDTOList(Collection<CarDO> drivers)
    {
        return drivers.stream()
            .map(CarMapper::makeCarDTO)
            .collect(Collectors.toList());
    }
}
