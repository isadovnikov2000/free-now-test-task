package com.mytaxi;

import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.driver.DriverService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

/**
 *
 */
public class DriverServiceTest extends AbstractTest {

  @Autowired
  private DriverService driverService;

  @Test
  public void createDriver() {
    DriverDO driverDO = new DriverDO(UUID.randomUUID().toString(), "test");

    driverService.create(driverDO);
  }

  @Test(expected = ConstraintsViolationException.class)
  public void createDriverException() {
    String username = getRandomString();
    DriverDO driverDO_1 = new DriverDO(username, "test");
    DriverDO driverDO_2 = new DriverDO(username, "test");

    driverService.create(driverDO_1);
    driverService.create(driverDO_2);
  }

  @Test
  public void findDriver() {
    DriverDO driverDO = new DriverDO(getRandomString(), "test");

    driverDO = driverService.create(driverDO);
    driverService.find(driverDO.getId());
  }

  @Test(expected = EntityNotFoundException.class)
  public void findDriverException() {
    driverService.find(-1L);
  }

  @Test
  public void deleteDriver() {
    DriverDO driverDO = new DriverDO(getRandomString(), "test");

    driverDO = driverService.create(driverDO);
    driverService.delete(driverDO.getId());
  }

  @Test(expected = EntityNotFoundException.class)
  public void deleteDriverException() {
    driverService.delete(-1L);
  }

}
